import React, {Component} from 'react';

import People from "./components/People/People";

import './App.css';

class App extends Component {
  state = {
    person: [
      {id: 1, name: "Ilia", age: 28, hobby: "Like games"},
      {id: 2, name: "John", age: 30, hobby: "Cooking"},
      {id: 3, name: "Anna", age: 25, hobby: "Programming"}
    ]
  };


  render() {
    return (
        <div className="App">
          <People people={this.state.person}/>
        </div>
    );
  }
}

export default App;