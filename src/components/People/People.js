import React from 'react';

import Person from "../Person/Person";

const People = props => {
    return props.people.map(person => {
        return <Person
            key={person.id}
            name={person.name}
            age={person.age}
        >
            {person.hobby}
        </Person>
    })
};

export default People;